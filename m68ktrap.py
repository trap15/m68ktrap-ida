"""
Copyright (C) 2014 Alex "trap15" Marshall <trap15@raidenii.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

"""
This plugin adds easier support for M68000 A-line and F-line traps.
Now it should be easy to use an enumerator for the actual trap values.

Code is a bit hacky, but should be nice enough to help people looking to write
an IDP hook in IDAPython.
"""

import idaapi
import idautils
import struct

#--------------------------------------------------------------------------
# Hook implementation
class m68ktrap_idp_hook_t(idaapi.IDP_Hooks):
  # Typical init boilerplate
  def __init__(self):
    idaapi.IDP_Hooks.__init__(self)

  # Custom analyzer
  def custom_ana(self):
    data = struct.unpack(">H", idaapi.get_many_bytes(idaapi.cmd.ea, 2))[0]
    # This is really hacky, should probably do something better.
    if data & 0xF000 == 0xF000 or data & 0xF000 == 0xA000: # FLINE or ALINE
      if data & 0xF000 == 0xF000: # FLINE
        idaapi.cmd.itype = idaapi.CUSTOM_CMD_ITYPE+0
      elif data & 0xF000 == 0xA000: # ALINE
        idaapi.cmd.itype = idaapi.CUSTOM_CMD_ITYPE+1
      idaapi.cmd.size = 2
      idaapi.cmd.Op1.type = idaapi.o_imm
      idaapi.cmd.Op1.dtyp = idaapi.dt_word
      idaapi.cmd.Op1.value = data
      idaapi.cmd.flags = idaapi.CF_USE1
      return True
    else: # Not supported by this module
      return False

  # Custom mnemonic printer
  def custom_mnem(self):
    s = None
    if idaapi.cmd.itype == idaapi.CUSTOM_CMD_ITYPE+0: # FLINE
      s = "_FTrap"
    elif idaapi.cmd.itype == idaapi.CUSTOM_CMD_ITYPE+1: # ALINE
      s = "_ATrap"
    else: # Unsupported
      s = None
    return s

#---------------------------------------------------------------------
# Hook installation/uninstallation
try: # Try to unhook (throws exception if not hooked yet)
  idp_hooked = False
  idphook
  idphook.unhook()
  del idphook
except: # Do hook if we couldn't unhook
  idp_hooked = True
  idphook = m68ktrap_idp_hook_t()
  idphook.hook()


if idp_hooked:
  idp_thisoper = "install"
  idp_nextoper = "uninstall"
else:
  idp_thisoper = "uninstall"
  idp_nextoper = "install"

print("M68ktrap %sed. Run again to %s." % (idp_thisoper, idp_nextoper))
